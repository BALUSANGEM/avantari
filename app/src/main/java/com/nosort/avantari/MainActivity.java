package com.nosort.avantari;

import android.animation.Animator;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gelitenight.waveview.library.WaveView;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.renderer.BarChartRenderer;
import com.github.mikephil.charting.renderer.YAxisRenderer;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private WaveHelper mWaveHelper;
    FrameLayout waveFrameLayout;
    FrameLayout chartFrameLayout;
    FloatingActionButton fab;
    ImageView backButtonImage;
    String TAG = MainActivity.class.getSimpleName();


    LineChart lineChart;
    BarChart barChart;

    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private int mBorderWidth = 1;


    TextView firstText;
    TextView secondText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstText = (TextView) findViewById(R.id.first_text);
        secondText = (TextView) findViewById(R.id.second_text);
        waveTextsActive();

        waveFrameLayout = (FrameLayout) findViewById(R.id.wave_layout);
        chartFrameLayout = (FrameLayout) findViewById(R.id.chart_layout);
        chartFrameLayout.setVisibility(View.INVISIBLE);
        backButtonImage = (ImageView) findViewById(R.id.back_button);

        final WaveView waveView = (WaveView) findViewById(R.id.wave);
        mWaveHelper = new WaveHelper(waveView);
        waveView.setShapeType(WaveView.ShapeType.SQUARE);

        waveView.setWaveColor(
                WaveView.DEFAULT_BEHIND_WAVE_COLOR,
                WaveView.DEFAULT_FRONT_WAVE_COLOR);
        mBorderColor = Color.parseColor("#44FFFFFF");
        waveView.setBorder(mBorderWidth, mBorderColor);


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Fab Clicked !", Toast.LENGTH_SHORT).show();
                TranslateAnimation animation = new TranslateAnimation(0, -400, 0, -400);
                animation.setInterpolator(new AccelerateInterpolator());
                animation.setDuration(160);
               // animation.setFillAfter(false);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {
                        showChartView();
                    }
                    @Override public void onAnimationRepeat(Animation animation) {}});
                fab.startAnimation(animation);
                chartTextsActive();
            }
        });


        backButtonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Back button clicked", Toast.LENGTH_SHORT).show();
                showWaveView();
                waveTextsActive();
            }
        });

        barChart = (BarChart) findViewById(R.id.bar_chart);
        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(2f, 60f));
        entries.add(new BarEntry(3f, 80f));
        entries.add(new BarEntry(4f, 110f));
        entries.add(new BarEntry(5f, 170f));
        entries.add(new BarEntry(6f, 90f));
        entries.add(new BarEntry(7f, 80f));
        entries.add(new BarEntry(8f, 200f));
        entries.add(new BarEntry(9f, 80f));
        entries.add(new BarEntry(10f, 110f));
        entries.add(new BarEntry(11f, 170f));
        entries.add(new BarEntry(12f, 70f));
        entries.add(new BarEntry(13f, 80f));
        entries.add(new BarEntry(14f, 140f));

        BarDataSet set = new BarDataSet(entries, "BarDataSet");
        /*set.setColor(R.color.white);
        set.setValueTextColor(R.color.white);*/
        BarData data = new BarData(set);
        IValueFormatter valueFormatter = new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                Log.d(TAG," Value is :: "+value);
                Log.d(TAG," Entry is :: x : "+entry.getX()+" y : "+entry.getY());
                Log.d(TAG," Data set index is : "+dataSetIndex);
                Log.d(TAG," View port handler is :: "+viewPortHandler.toString());
                return "";
            }
        };

        data.setValueFormatter(valueFormatter);
        data.setDrawValues(true);
        data.setBarWidth(0.1f); // set custom bar width
        barChart.setData(data);

        YAxis leax = barChart.getAxisLeft();
        leax.setLabelCount(2);
        leax.setDrawTopYLabelEntry(true);

        YAxis riax = barChart.getAxisRight();
        riax.setDrawLabels(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawAxisLine(false);
       // ChartAnimator chartAnimator = new ChartAnimator();
     //   chartAnimator.animateY(1000);

        Description description = new Description();
        description.setText(" ");
        barChart.setDescription(description);
        barChart.setFitBars(true); // make the x-axis fit exactly all bars
        barChart.invalidate(); // refresh


    }
    private void showChartView(){
        chartFrameLayout.setVisibility(View.VISIBLE);
        barChart.setVisibility(View.GONE);
        Log.d(TAG," ChartFrame 1 : "+chartFrameLayout.getVisibility()+" visible : "+View.VISIBLE+" gone : "+View.GONE+" invisible : "+View.INVISIBLE);

        int startradius=fab.getHeight()/2;
        int endradius = Math.max(chartFrameLayout.getWidth(), chartFrameLayout.getHeight());
        int cx = (chartFrameLayout.getLeft() + chartFrameLayout.getRight()) / 2;
        int cy = (chartFrameLayout.getTop() + chartFrameLayout.getBottom()) / 2;

        Animator animator = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            animator = ViewAnimationUtils.createCircularReveal(chartFrameLayout, cx, cy, startradius, endradius);
            animator.addListener(new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animation) {
                    Log.d(TAG," ChartFrame 2 : "+chartFrameLayout.getVisibility());
                    chartFrameLayout.setVisibility(View.VISIBLE);
                    Log.d(TAG," ChartFrame 3 : "+chartFrameLayout.getVisibility());
                    fab.setVisibility(View.GONE);
                }
                @Override
                public void onAnimationEnd(Animator animation) {
                    Log.d(TAG," ChartFrame 4 : "+chartFrameLayout.getVisibility());
                    waveFrameLayout.setVisibility(View.GONE);
                    backButtonImage.setVisibility(View.VISIBLE);
                    barChart.setVisibility(View.VISIBLE);
                    barChart.animateXY(1000,1000);}
                @Override public void onAnimationCancel(Animator animation) {}
                @Override public void onAnimationRepeat(Animator animation) {}
            });
            animator.setInterpolator(new AccelerateInterpolator());
            animator.setDuration(300);
            Log.d(TAG,"Get Visibility of chartframe : "+chartFrameLayout.getVisibility());
            animator.start();
        }

    }

    private void showWaveView() {
        backButtonImage.setVisibility(View.GONE);
        waveFrameLayout.setVisibility(View.VISIBLE);

        int endradius=fab.getHeight()/2;
        int startradius = Math.max(chartFrameLayout.getWidth(), chartFrameLayout.getHeight());
        int cx = (chartFrameLayout.getLeft() + chartFrameLayout.getRight()) / 2;
        int cy = (chartFrameLayout.getTop() + chartFrameLayout.getBottom()) / 2;

        Animator animator = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            animator = ViewAnimationUtils.createCircularReveal(chartFrameLayout, cx, cy, startradius, endradius);
            animator.addListener(new Animator.AnimatorListener() {
                @Override public void onAnimationStart(Animator animation) {}
                @Override
                public void onAnimationEnd(Animator animation) {
                    chartFrameLayout.setVisibility(View.GONE);
                    reshowFab();
                }
                @Override public void onAnimationCancel(Animator animation) {}
                @Override public void onAnimationRepeat(Animator animation) {}
            });
            animator.setInterpolator(new AccelerateInterpolator());
            animator.setDuration(300);
            animator.start();
        }
    }

    private void reshowFab(){fab.setVisibility(View.VISIBLE);
        TranslateAnimation animation = new TranslateAnimation(-400, -0, -400, 0);
        animation.setInterpolator(new AccelerateInterpolator());
        animation.setDuration(200);
        // animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation) {
             //   showChartView();
            }
            @Override public void onAnimationRepeat(Animation animation) {}});
        fab.startAnimation(animation);
    }
    private void waveTextsActive(){
        firstText.setText("WaveText");
        secondText.setText("180.9kWh");
    }
    private void chartTextsActive(){
        firstText.setText("ChartText");
        secondText.setText("92.3kWh");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWaveHelper.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWaveHelper.start();
    }
}